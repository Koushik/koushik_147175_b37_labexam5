<?php
class MyCalculator {
    private $_first_value, $_second_value;
    public function __construct( $first_value, $second_value ) {
        $this->_first_value = $first_value;
        $this->_second_value = $second_value;
    }
    public function add() {
        return $this->_first_value + $this->_second_value;
    }
    public function subtract() {
        return $this->_first_value - $this->_second_value;
    }
    public function multiply() {
        return $this->_first_value * $this->_second_value;
    }
    public function divison() {
        return $this->_first_value / $this->_second_value;
    }
}
$mycalculator = new MyCalculator(12, 6);
echo $mycalculator-> add();
echo"<br>";
echo $mycalculator-> multiply();
echo"<br>";
echo $mycalculator->subtract();
echo"<br>";
echo $mycalculator->divison();

?>