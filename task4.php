<?php
class factorial_of_a_number
{
    protected $_n;
    public function __construct($n)
    {
        if (!is_int($n))
        {
            throw new InvalidArgumentException('Not a number or missing argument');
        }
        $this->_n = $n;
    }
    public function result()
    {
        $factorial = 1;
        for ($k = 1; $k <= $this->_n; $k++)
        {
            $factorial *= $k;
        }
        return $factorial;
    }
}

$newfactorial = New factorial_of_a_number(5);
echo $newfactorial->result();
?>